// ==UserScript==
// @name          Open with VRCX
// @namespace     Violentmonkey Scripts
// @description   Adds buttons to vrchat user/world pages that open the page in VRCX
// @version       1.0.0
// @author        Teacup
// @match         https://vrchat.com/home/*
// @match         https://vrclist.com/*
// @require       https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js
// @require       https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js
// @require       https://gist.github.com/raw/2625891/waitForKeyElements.js
// @run-at        document-start
// ==/UserScript==


/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
var __webpack_exports__ = {};

// Whether to automatically open the world/user page in VRCX upon page load
const AUTO_OPEN = false;
// The local favorite list that worlds will be added to
let FAVORITE_LIST = localStorage.getItem('vrcx_favoriteList') ?? 'TODO';
function doAutoOpen(id, world) {
    if (AUTO_OPEN) {
        console.log('Auto opening', id, 'in VRCX');
        let endpoint = world ? 'world' : 'user';
        window.location.href = `vrcx://${endpoint}/${id}`;
    }
}
const url = new URL(window.location.href);
let worldCards = new Array();
let firstLoad = true;
function processUrl(url) {
    console.log('Processing new URL', url.href);
    switch (url.hostname) {
        case 'vrchat.com':
            processVRCUrl(url);
            break;
        case 'vrclist.com':
            processVRCListUrl(url);
            break;
        default:
            console.log('Invalid url hostname?');
    }
}
function processVRCListUrl(url) {
    if (url.pathname.startsWith('/world')) {
        waitForKeyElements('world-page', () => { setTimeout(() => addWorldBtn_VrcList(), 1000); }, true);
    }
}
function processVRCUrl(url) {
    let openUrl = null;
    let id = url.pathname.split('/').pop();
    if (url.pathname.startsWith('/home/world/')) {
        waitForKeyElements('a:contains("Public Link")', addWorldBtn, true);
        doAutoOpen(id, true);
    }
    else if (url.pathname.startsWith('/home/launch')) {
        waitForKeyElements('button:contains("INVITE ME")', addInstanceBtn, true);
    }
    else if (url.pathname.startsWith('/home/user/')) {
        waitForKeyElements('.user-info', addUserBtn, true);
        doAutoOpen(id, false);
    }
    else {
        console.log('No button to add on this page');
    }
}
function addUserBtn() {
    console.log('Adding VRCX button to user page');
    // Get the last social icon in the sheet
    var socialIcon = $('a.social-icon').last();
    var buttonParent = socialIcon.parent();
    var newButton = buttonParent.clone();
    var newButtonLink = newButton.children('a');
    newButtonLink.attr('href', 'vrcx://user/' + url.pathname.split('/').pop());
    // make button open link without opening a whole new tab
    newButtonLink.attr('target', '_self');
    // get social-svg child
    var container = newButtonLink.children('.social-container');
    var socialSvg = container.children('svg.social-svg');
    socialSvg.attr('viewBox', '0 0 64 64');
    socialSvg.attr("preserveAspectRatio", "xMidYMid meet");
    // Create scuffed vrcx icon
    var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    g.setAttribute('transform', 'translate(0.000000,64.000000) scale(0.100000,-0.100000)');
    g.setAttribute('fill', '#000000');
    g.setAttribute('stroke', 'none');
    g.setAttribute("class", "social-svg-mask");
    g.setAttribute("style", "transition: fill 170ms ease-in-out 0s; fill: rgb(0, 0, 0);");
    var path1 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    path1.setAttribute('d', 'M0 320 l0 -320 320 0 320 0 0 320 0 320 -320 0 -320 0 0 -320z m582 273 c16 -14 18 -34 18 -193 0 -185 -4 -205 -45 -212 -18 -3 -20 -12 -23 -81 -2 -52 -7 -77 -15 -76 -7 0 -39 35 -72 77 l-60 77 -160 3 c-96 1 -164 7 -172 13 -18 15 -19 379 -1 397 9 9 81 12 262 12 218 0 252 -2 268 -17z');
    g.appendChild(path1);
    var path2 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    path2.setAttribute('d', 'M187 528 c4 -7 24 -37 45 -67 21 -30 38 -58 38 -63 0 -5 -20 -38 -45 -72 -25 -35 -45 -66 -45 -70 0 -3 16 -6 35 -6 31 0 40 6 67 45 18 25 34 45 37 45 3 0 19 -20 34 -45 26 -41 32 -45 68 -45 21 0 39 3 39 8 0 4 -20 36 -45 71 -25 36 -45 68 -45 73 0 5 17 32 38 61 59 80 58 77 14 77 -35 0 -43 -5 -67 -40 -15 -22 -31 -40 -34 -40 -3 0 -17 18 -31 40 -23 37 -28 40 -68 40 -32 0 -41 -3 -35 -12z');
    g.appendChild(path2);
    // Remove old icon, add new one
    socialSvg.children('g').remove();
    socialSvg.append(g);
    // Add the new link after the last social icon
    buttonParent.after(newButton);
}
function addInstanceBtn() {
    console.log('Adding VRCX button to instance page');
    let btn = $('button:contains("INVITE ME")');
    let h2 = btn.parent();
    let newButton = h2.clone();
    let newButtonLink = newButton.children('button');
    let worldId = url.searchParams.get('worldId');
    let instanceId = url.searchParams.get('instanceId');
    let shortName = url.searchParams.get('shortName') ?? url.searchParams.get('secureName');
    let vrcxLink = undefined;
    if (shortName !== null) {
        vrcxLink = `vrcx://world/${shortName}`;
    }
    else if (instanceId !== null && instanceId !== '0') {
        vrcxLink = `vrcx://world/${worldId}:${instanceId}`;
    }
    else {
        vrcxLink = `vrcx://world/${worldId}`;
    }
    newButton.on('click', function () {
        console.log('Opening VRCX link', vrcxLink);
        window.location.href = vrcxLink;
    });
    newButtonLink.text('Open with VRCX');
    // Add the new link after the old one
    h2.after(newButton);
}
function addWorldBtn() {
    console.log('Adding VRCX button to world page');
    // We're just gonna copy this lol
    var link = $('a:contains("Public Link")');
    var newLink = link.clone();
    newLink.attr('href', 'vrcx://world/' + window.location.href.split('/').pop());
    newLink.text('Open with VRCX');
    // make button open link without opening a whole new tab (i thought this one already didn't do that..)
    newLink.attr('target', '_self');
    // Add the new link after the old one
    link.after(newLink);
}
function addWorldBtn_VrcList() {
    let page = $("world-page")[0];
    let shadow = page.shadowRoot;
    let link = $("a#world-link", shadow);
    let newLink = link.clone();
    console.log("Adding button to vrclist page", shadow, link, newLink);
    // Get the world ID from the existing link href
    console.log("Current href", link.attr("href"));
    let worldId = link.attr("href").split('/').pop();
    newLink.attr('href', 'vrcx://world/' + worldId);
    newLink.attr('id', 'vrcx-world-link');
    newLink.attr('target', '_self');
    newLink.text('Open with VRCX');
    processWorldCard($(page), worldId);
    link.after(newLink);
}
function doWorldCardButtons(card, id) {
    if (worldCards.length === 0) {
        return;
    }
    console.log(`Adding VRCX buttons to ${worldCards.length} world cards`);
    worldCards.forEach(function (element, idx) {
        processWorldCard(element);
    });
    worldCards = new Array();
}
function processWorldCard(element, id) {
    let card = $(element);
    let worldId = id ?? card.children("span[slot='world-id']").text();
    //console.log(`Adding VRCX button to world card ${idx} with ID ${worldId}`, card)
    if (worldId === undefined)
        return;
    let visitedButton = $("button#visited-circle", card[0].shadowRoot);
    let newButton = visitedButton.clone();
    newButton.attr("id", "vrcx-circle");
    newButton.attr("title", `Open with VRCX (Shift-Click to local favorite to '${FAVORITE_LIST}' list)`);
    // copy css from old button
    let style = getComputedStyle(visitedButton[0]);
    let left = parseFloat(style.left) + 32;
    // Copy styles from getComputedStyle to new button
    let styles = {};
    let btn = newButton[0];
    switch (style.constructor.name) {
        // Fix for firefox
        case 'CSS2Properties':
            Object.values(style).forEach((prop) => {
                var val = style.getPropertyValue(prop);
                if (val === "" || val === undefined || val === null)
                    return;
                // @ts-ignore
                btn.style[prop] = val;
            });
            break;
        case 'CSSStyleDeclaration':
            Object.assign(btn.style, style);
            break;
        default:
            console.error('Unknown style object prototype');
            break;
    }
    newButton.css("left", left + "px");
    newButton.on("mouseover", function () {
        newButton.css("cursor", "pointer");
        newButton.css("transform", "scale(1.1)");
    });
    newButton.on("mouseout", function () {
        newButton.css("cursor", "default");
        newButton.css("transform", "scale(1)");
    });
    let icon = newButton.children("img");
    icon.attr("id", "vrcx-icon");
    icon.attr("src", "https://raw.githubusercontent.com/vrcx-team/VRCX/master/VRCX.ico");
    // limit img size to 24x24px
    icon.attr("style", "width: 24px; height: 24px;");
    newButton.on("click", function (event) {
        // Check if shift is being held down
        if (event.shiftKey) {
            // Prompt user for list name
            let list = prompt("Enter local favorite list name", FAVORITE_LIST);
            if (list === null)
                return;
            FAVORITE_LIST = list;
            console.log("Favoriting world in VRCX", worldId, FAVORITE_LIST);
            localStorage.setItem("vrcx_favoriteList", FAVORITE_LIST);
            // Open in VRCX
            window.location.href = `vrcx://local-favorite-world/${worldId}:${FAVORITE_LIST}`;
        }
        else {
            console.log("Opening world in VRCX", worldId);
            window.location.href = `vrcx://world/${worldId}`;
        }
    });
    visitedButton.after(newButton);
}
// Since the website is a SPA, we need to check for URL changes
setInterval(() => {
    if (url.href !== window.location.href) {
        url.href = window.location.href;
        console.log("New URL: " + url.href);
        processUrl(url);
    }
    if (worldCards.length > 0) {
        doWorldCardButtons();
    }
}, 500);
// Detour attachShadow so that we can get all world-card elements without having to traverse shadow DOM. *shudder*
if (url.hostname === "vrclist.com") {
    console.log("Detouring attachShadow");
    let originalAttachShadow = Element.prototype.attachShadow;
    Element.prototype.attachShadow = function (opts) {
        let name = this.tagName;
        if (name.toLowerCase() === "world-card" || name.toLowerCase() === "world-card-ron" || (name.toLowerCase() === "div" && this.getAttribute("id") === "world")) {
            worldCards.push($(this));
        }
        return originalAttachShadow.call(this, opts);
    };
}
processUrl(url);

/******/ })()
;